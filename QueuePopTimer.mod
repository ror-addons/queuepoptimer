<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="QueuePopTimer" version="1.0" date="11/30/2019" >        
        <Author name="Elabas"  />
        <Description text="Shows you your estimated queue time" />
        <Dependencies>
            <Dependency name="EA_ChatWindow" />
        </Dependencies>
        <Files>
            <File name="QueuePopTimer.lua"/>
        </Files>
        <OnInitialize>
            <CallFunction name="QueuePopTimer.Initialize" />
        </OnInitialize>
		<OnUpdate>
            <CallFunction name="QueuePopTimer.Update"/>
		</OnUpdate>
        <OnShutdown/>
    </UiMod>
</ModuleFile>
