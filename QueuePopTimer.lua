if not QueuePopTimer then QueuePopTimer = {} end

local find = find
local match = match
local tonumber = tonumber

QueuePopTimer.queueTime = 0
QueuePopTimer.queuePopTime = 0
QueuePopTimer.elapsedTime = 0
QueuePopTimer.currentWaitTimeNormal = L"no data"
QueuePopTimer.currentWaitTimeSoloRanked = L"no data"
QueuePopTimer.currentWaitTimePremadeRanked = L"no data"

QueuePopTimer.queueType = {
	Normal = L"N",
	SoloRanked = L"S",
	PremadeRanked = L"G",
}

QueuePopTimer.careers = {
    [GameData.CareerLine.WITCH_ELF] = L"DPS",
    [GameData.CareerLine.BRIGHT_WIZARD]  = L"DPS",
    [GameData.CareerLine.CHOPPA]  = L"DPS",
    [GameData.CareerLine.ENGINEER]  = L"DPS",
    [GameData.CareerLine.SLAYER]  = L"DPS",
    [GameData.CareerLine.MAGUS]  = L"DPS",
    [GameData.CareerLine.WHITE_LION]  = L"DPS",
    [GameData.CareerLine.SHADOW_WARRIOR]  = L"DPS",
    [GameData.CareerLine.SORCERER]  = L"DPS",
    [GameData.CareerLine.SQUIG_HERDER]  = L"DPS",
    [GameData.CareerLine.MARAUDER]  = L"DPS",
    [GameData.CareerLine.WITCH_HUNTER]  = L"DPS",
    [GameData.CareerLine.ARCHMAGE] = L"Heal",
    [GameData.CareerLine.DISCIPLE] = L"Heal",
    [GameData.CareerLine.RUNE_PRIEST] = L"Heal",
    [GameData.CareerLine.WARRIOR_PRIEST] = L"Heal",
    [GameData.CareerLine.SHAMAN] = L"Heal",
    [GameData.CareerLine.ZEALOT] = L"Heal",
    [GameData.CareerLine.BLACK_ORC] = L"Tank",
    [GameData.CareerLine.CHOSEN] = L"Tank",
    [GameData.CareerLine.IRON_BREAKER] = L"Tank",
    [GameData.CareerLine.SWORDMASTER] = L"Tank",
    [GameData.CareerLine.BLACKGUARD] = L"Tank",
    [GameData.CareerLine.KNIGHT] = L"Tank",
}

QueuePopTimer.waitTimesNormal = {}
QueuePopTimer.waitTimesSoloRanked = {}
QueuePopTimer.waitTimesPremadeRanked = {}

local function Print(str)
	EA_ChatWindow.Print(towstring(str))
end

function QueuePopTimer.Initialize()
	RegisterEventHandler(SystemData.Events.SCENARIO_ACTIVE_QUEUE_UPDATED, "QueuePopTimer.UpdateScenarioQueue")
	RegisterEventHandler(SystemData.Events.SCENARIO_SHOW_JOIN_PROMPT, "QueuePopTimer.UpdateScenarioPop")
	RegisterEventHandler( TextLogGetUpdateEventId("Chat"), "QueuePopTimer.OnChatLogUpdated");


	Print(L"<LINK data=\"0\" text=\"[QueuePopTimer]\" color=\"50,255,10\"> Addon initialized.");
end

--/script QueuePopTimer.UpdateScenarioQueue()
function QueuePopTimer.UpdateScenarioQueue()
	local queueData = GetScenarioQueueData()
	if (queueData ~= nil and queueData.totalQueuedScenarios >= 1) then
		QueuePopTimer.queueTime = GetComputerTime()
	end	
end
--/script QueuePopTimer.UpdateScenarioPop()
function QueuePopTimer.UpdateScenarioPop()
	QueuePopTimer.queuePopTime = GetComputerTime()
	local name = GetScenarioName( GameData.ScenarioData.startingScenario )
	local queueType = L""
	if(name == "Caledor Woods (Ranked Solo)")then
		queueType = QueuePopTimer.queueType.SoloRanked
	elseif (name == "Caledor Woods (Ranked)") then
	    queueType = QueuePopTimer.queueType.PremadeRanked
	else
		queueType = QueuePopTimer.queueType.Normal
	end

	if(QueuePopTimer.queueTime <= QueuePopTimer.queuePopTime) then
		QueuePopTimer.elapsedTime = QueuePopTimer.queuePopTime - QueuePopTimer.queueTime
	else
		--a new day has begun while we waited
		QueuePopTimer.elapsedTime = QueuePopTimer.queuePopTime + (86400 - QueuePopTimer.queueTime)
	end

	QueuePopTimer.SendChatMessage(queueType)
end


function QueuePopTimer.SendChatMessage(queueType)

	local timer, Time = 0, QueuePopTimer.elapsedTime
	local dSec = Time % 60
	Time = (Time - dSec) / 60
	local dMin = Time % 60
	Time = (Time - dMin) / 60
	local dHour = Time

	if dHour == 24 then dHour = 0 end

	local message = L"QPT,"
	if(GameData.Player.level <= 16) then
		message = message .. L"T1,"
	elseif (GameData.Player.level <= 40) then
	    message = message .. L"T3,"
	else
		message = message .. L"T4,"
	end

	message = message .. QueuePopTimer.GetRole() ..L","
	message = message .. queueType .. L":"
	message = message .. wstring.format(L"%02d:%02d:%02d", dHour, dMin, dSec)
	EA_ChatWindow.Print(message, SystemData.ChatLogFilters.CHANNEL_9)

end

function QueuePopTimer.GetRole()

	if (QueuePopTimer.careers[GameData.Player.career.line] == L"Heal" or QueuePopTimer.careers[GameData.Player.career.line] == L"Tank") then
		local masteryPoints = QueuePopTimer.LoadMasteryPoints()
		if (GameData.Player.career.line == GameData.CareerLine.IRON_BREAKER) then
			if masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif  (GameData.Player.career.line == GameData.CareerLine.RUNE_PRIEST) then
		    if masteryPoints[2] <= masteryPoints[1] or masteryPoints[2] <= masteryPoints[1] then
				return L"Heal"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.KNIGHT) then
			if masteryPoints[1] <= masteryPoints[2] or masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.WARRIOR_PRIEST) then
			if masteryPoints[1] <= masteryPoints[3] or masteryPoints[1] <= masteryPoints[3] then
				return L"Heal"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.ARCHMAGE) then
			if masteryPoints[1] <= masteryPoints[3] or masteryPoints[1] <= masteryPoints[3] then
				return L"Heal"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.SWORDMASTER) then
			if masteryPoints[1] <= masteryPoints[2] or masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.CHOSEN) then
			if masteryPoints[1] <= masteryPoints[2] or masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.ZEALOT) then
			if masteryPoints[2] <= masteryPoints[1] or masteryPoints[2] <= masteryPoints[1] then
				return L"Heal"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.BLACK_ORC) then
			if masteryPoints[1] <= masteryPoints[2] or masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.SHAMAN) then
			if masteryPoints[1] <= masteryPoints[3] or masteryPoints[1] <= masteryPoints[3] then
				return L"Heal"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.BLACKGUARD) then
			if masteryPoints[1] <= masteryPoints[3] then
				return L"Tank"
			end
		elseif (GameData.Player.career.line == GameData.CareerLine.DISCIPLE) then
			if masteryPoints[1] <= masteryPoints[3] or masteryPoints[1] <= masteryPoints[3] then
				return L"Heal"
			end
		end
		return L"DPS"
	else
		return QueuePopTimer.careers[GameData.Player.career.line]
	end

end

function QueuePopTimer.LoadMasteryPoints()
    QueuePopTimer.advanceData = GameData.Player.GetAdvanceData()
    local masteryPoints = {0,0,0}
    -- Populate the current and initial advance levels
    for index, advanceData in pairs(QueuePopTimer.advanceData)
    do
        if (advanceData.category == GameData.CareerCategory.SPECIALIZATION)
        then
        	if(advanceData.packageId == 1 or advanceData.packageId == 2) then
        		masteryPoints[advanceData.packageId] = advanceData.timesPurchased
        	elseif advanceData.packageId == 3 then
        	    masteryPoints[advanceData.packageId] = advanceData.timesPurchased
        	    return masteryPoints
        	end
        end
    end
end

function QueuePopTimer.OnChatLogUpdated(updateType, filterType)
	if (updateType ~= SystemData.TextLogUpdate.ADDED) then return end
	if (filterType ~= SystemData.ChatLogFilters.CHANNEL_9) then return end

	local _, filterId, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 );
	text = towstring(text);


	if not text:find(L"QPT") then return end

	local tier,archetype,queueType,hour,mini,sec = text:match( L"QPT,([^%.]-),([^%.]-),(%u-):(%d-):(%d-):(%d+)" );
	if(tier == nil) then
		--old format
		tier,archetype,hour,mini,sec = text:match( L"QPT,([^%.]-),([^%.]-),(%d-):(%d-):(%d+)" );
		queueType = L"N"
	end


	local playerTier = ""
	hour = tonumber(hour)
	mini = tonumber(mini)
	sec = tonumber(sec)

	if(GameData.Player.level <= 16) then
		playerTier = L"T1"
	elseif (GameData.Player.level <= 40) then
	    playerTier =  L"T3"
	else
		playerTier =  L"T4"
	end

	if (playerTier ~= L"T4" or (playerTier == tier and QueuePopTimer.GetRole() == archetype)) then 
		local timeInSec = (hour * 60 * 60) + (mini * 60) + sec
		if(queueType == QueuePopTimer.queueType.Normal) then
			QueuePopTimer.currentWaitTimeNormal = QueuePopTimer.CalcWaitTimes(QueuePopTimer.waitTimesNormal)
			QueuePopTimer.CleanWaitTimes(QueuePopTimer.waitTimesNormal)
		elseif queueType == QueuePopTimer.SoloRanked then
		 	QueuePopTimer.currentWaitTimeSoloRanked = QueuePopTimer.CalcWaitTimes(QueuePopTimer.waitTimesSoloRanked)
		 	QueuePopTimer.CleanWaitTimes(QueuePopTimer.waitTimesSoloRanked)
		else
			QueuePopTimer.currentWaitTimePremadeRanked = QueuePopTimer.CalcWaitTimes(QueuePopTimer.waitTimesPremadeRanked)
			QueuePopTimer.CleanWaitTimes(QueuePopTimer.waitTimesPremadeRanked)
		end
		
	end

end

function QueuePopTimer.CalcWaitTimes(timesList)
	table.insert(timesList, {time = timeInSec, added = GetComputerTime()})

	local meanTime = QueuePopTimer.mean(timesList)
	local timer, Time = 0, meanTime
	local dSec = Time % 60
	Time = (Time - dSec) / 60
	local dMin = Time % 60
	Time = (Time - dMin) / 60
	local dHour = Time

	
	if (dHour == 0 and dMin == 0) then
		return wstring.format(L"%02d", dSec) .. L" sec"
	elseif (dHour == 0) then
	    return wstring.format(L"%02d:%02d", dMin, dSec) .. L" min"
	else
		return wstring.format(L"%02d:%02d:%02d", dHour, dMin, dSec) .. L" h"
	end
end

function QueuePopTimer.CleanWaitTimes(timesList)
	local removedKeys = {}
	local time = GetComputerTime()
	for k,v in pairs(timesList) do
    	if(time >= v.added) then
			if(time - v.added >= 900) then
				table.insert(removedKeys,k)
			end
		else
			--a new day has begun while we waited
			local elapsedTime = time + (86400 - v.added)
			if(elapsedTime >= 900) then
				table.insert(removedKeys,k)
			end
		end
  	end

  	for _,v in pairs(removedKeys) do
  		timesList[v] = nil
  	end
end

function QueuePopTimer.Update()
	if(Tooltips.curMouseOverWindow == "CMapWindowMapScenarioQueue") then
		local message = L"You are queued as " .. QueuePopTimer.GetRole() .. L"\nEstimated wait times:\n"
		message = message .. L"Normal queue: " ..  QueuePopTimer.currentWaitTimeNormal .. L"\n"
		if(GameData.Player.level == 40) then
			message = message .. L"Solo Ranked queue: " ..  QueuePopTimer.currentWaitTimeSoloRanked .. L"\n"
			message = message .. L"Premade Ranked queue: " ..  QueuePopTimer.currentWaitTimePremadeRanked .. L"\n" 
		end
		Tooltips.SetExtraText("DefaultTooltip", "ActionText", "ActionTextLine", message, nil)
		Tooltips.Finalize()
    end
end

function QueuePopTimer.mean( t )
  local sum = 0
  local count= 0

  for k,v in pairs(t) do
    if type(v.time) == 'number' then
      sum = sum + v.time
      count = count + 1
    end
  end

  return (sum / count)
end

